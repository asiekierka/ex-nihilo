package exnihilo.compatibility.foresty;

import java.util.HashMap;
import java.util.Map;

import forestry.api.apiculture.FlowerManager;
import forestry.api.genetics.IIndividual;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class Surrounding {
	public Map<String, Integer> blocks = new HashMap<String, Integer>();
	public Map<String, Integer> flowers = new HashMap<String, Integer>();
	public int leafCount;

	public String blockAbove;
	
	public void addBlock(World world, int x, int y, int z)
	{
		Block block = world.getBlock(x, y, z);
	    int meta = world.getBlockMetadata(x, y, z);
		
		if (block != null && block.isLeaves(world, x, y, z))
		{
			leafCount++;
		}
		
		String key = block + ":" + meta;
		
		if (blocks.containsKey(key))
		{
			int count = blocks.get(key);
			
			blocks.put(key, count + 1);
		}else
		{
			blocks.put(key, 1);
		}
		
		tryAddFlower(world, null, x, y, z);
	}
	
	public void setBlockAbove(Block block, int meta)
	{
		this.blockAbove = block + ":" + meta;
	}
	
	public void tryAddFlower(World world, IIndividual individual, int x, int y, int z)
	{
		if (FlowerManager.flowerRegistry.isAcceptedFlower(FlowerManager.FlowerTypeVanilla, world, individual, x, y, z))
			addFlower(FlowerType.Normal);
		
		if (FlowerManager.flowerRegistry.isAcceptedFlower(FlowerManager.FlowerTypeNether, world, individual, x, y, z))
			addFlower(FlowerType.Nether);
		
		if (FlowerManager.flowerRegistry.isAcceptedFlower(FlowerManager.FlowerTypeEnd, world, individual, x, y, z))
			addFlower(FlowerType.End);
		
		if (FlowerManager.flowerRegistry.isAcceptedFlower(FlowerManager.FlowerTypeJungle, world, individual, x, y, z))
			addFlower(FlowerType.Jungle);
		
		if (FlowerManager.flowerRegistry.isAcceptedFlower(FlowerManager.FlowerTypeMushrooms, world, individual, x, y, z))
			addFlower(FlowerType.Mushroom);
		
		if (FlowerManager.flowerRegistry.isAcceptedFlower(FlowerManager.FlowerTypeCacti, world, individual, x, y, z))
			addFlower(FlowerType.Cactus);
		
		if (FlowerManager.flowerRegistry.isAcceptedFlower(FlowerManager.FlowerTypeGourd, world, individual, x, y, z))
			addFlower(FlowerType.Gourd);
		
		if (world.getBlock(x, y, z) == Blocks.waterlily)
			addFlower(FlowerType.Water);
	}
	
	private void addFlower(FlowerType type)
	{
		String key = type.name();
		
		if (flowers.containsKey(key))
		{
			int count = flowers.get(key);
			
			flowers.put(key, count + 1);
		}else
		{
			flowers.put(key, 1);
		}
	}
	
	public int getFlowerCount(FlowerType type)
	{
		String key = type.name();
		
		switch (type)
		{
		case None:
			return 0;
		
		default:
			if (flowers.containsKey(key))
			{
				return flowers.get(key);
			}
			else
			{
				return 0;
			}
		}
	}
}
