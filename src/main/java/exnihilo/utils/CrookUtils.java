package exnihilo.utils;

import java.lang.reflect.Method;
import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import cpw.mods.fml.common.Loader;
import exnihilo.ENBlocks;
import exnihilo.ENItems;
import exnihilo.ExNihilo;
import exnihilo.data.ModData;

public class CrookUtils {

	private static Class forestryLeafBlock = null;
	private static Method dropStuff = null;
	
	public static boolean doCrooking(ItemStack item, int X, int Y, int Z, EntityPlayer player)
	{
		return doCrooking(item, X, Y, Z, player, true);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static boolean doCrooking(ItemStack item, int X, int Y, int Z, EntityPlayer player, boolean damage)
	{
		World world = player.worldObj;
		Block block = world.getBlock(X,Y,Z);
		int meta = world.getBlockMetadata(X, Y, Z);
		boolean validTarget = false;
		boolean extraDropped = false;

		if (block.isLeaves(world,X,Y,Z))
		{
			if (!world.isRemote)
			{
				if (Loader.isModLoaded("Forestry"))
				{
					//Forestry, why? Why did you make me have to do this? We could have been friends...
					
					try {
						if (forestryLeafBlock == null)
							forestryLeafBlock = Class.forName("forestry.arboriculture.gadgets.ForestryBlockLeaves");
						if (forestryLeafBlock != null)
						{
							if (dropStuff == null)
							{
								dropStuff = forestryLeafBlock.getDeclaredMethod("getLeafDrop", World.class, int.class, int.class, int.class, float.class, int.class);
								dropStuff.setAccessible(true);
							}
						} 
						else 
						{
							ExNihilo.log.error("getLeafDrop == null");
						}

						if (dropStuff != null)
						{
							ArrayList<ItemStack> drops = (ArrayList<ItemStack>) dropStuff.invoke(forestryLeafBlock.newInstance(), world, X, Y, Z, 1.0f, 1);
							if (drops != null) 
							{
								for (ItemStack drop : drops) 
								{
									System.out.println("TESTING");
									world.spawnEntityInWorld(new EntityItem(world, X + 0.5D, Y + 0.5D, Z + 0.5D, drop));
								}
							}
							extraDropped = true;
						}
						else
						{
							ExNihilo.log.error("dropStuff == null");
						}
					}
					catch (Exception ex)
					{
						ExNihilo.log.error("Failed to get getLeafDrop from Forestry ForestryBlockLeaves class");
						ex.printStackTrace();
					}
				}

				//If the Forestry method didn't work, try the vanilla way.
				if (!extraDropped)
				{
					//Call it once here and it gets called again when it breaks. 
					block.dropBlockAsItem(world, X, Y, Z, meta, 0);
				}


				//Silkworms
				if (ModData.ALLOW_SILKWORMS && world.rand.nextInt(100) == 0)
				{
					world.spawnEntityInWorld(new EntityItem(world, X + 0.5D, Y + 0.5D, Z + 0.5D, new ItemStack(ENItems.Silkworm, 1, 0)));
				}
			}

			validTarget = true;
		}

		if (block == ENBlocks.LeavesInfested)
		{
			if (!world.isRemote)
			{
				if (ModData.ALLOW_SILKWORMS && world.rand.nextInt(15) == 0)
				{
					world.spawnEntityInWorld(new EntityItem(world, X + 0.5D, Y + 0.5D, Z + 0.5D, new ItemStack(ENItems.Silkworm, 1, 0)));
				}
			}

			validTarget = true;
		}

		if (validTarget)
		{
			if (damage)
				item.damageItem(1, player);

			if (item.stackSize == 0)
			{
				player.destroyCurrentEquippedItem();
			}
		}
		
		if (!world.isRemote)
		{
			world.func_147480_a(X, Y, Z, true);
		}

		return validTarget;
	}

	
}
